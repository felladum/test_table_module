import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {TableComponent} from "./modules/simple-grid/components/table/table.component";
import {DataProvider} from "./modules/simple-grid/data-types";
import {ApiService} from "./services/api.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ang-table';
  public DS :DataProvider = {
    header:[
      {
        name: 'Id',
        field: 'id',
        filter: true,
      },
      {
        name: 'wbRating',
        field: 'id',
        filter: true,
      },
      {
        name: 'Отзывов (ф: не больше чем)',
        field: 'reviewsCount',
        filter: function (el, val) {
          return parseInt(el) < parseInt(val) // проверим кастомный поиск
        },
      },
      {
        name: 'sku',
        field: 'sku',
        filter: false
      },
      {
        name: 'Наименование',
        field: 'name',
        filter: true
      },
    ],
    values:[],
    pageSize: 5
  }

  constructor(private api: ApiService, private ref: ChangeDetectorRef) {
  }

  ngOnInit(){
    this.loadData();
  }

  loadData(){
    this.api.getTableData().subscribe((res: any)=>{
      // ой как грязно. надо или разделить заголовок/данные/на странице, и тогда простыми push, и изменениями простых значений менять,
      // либо как сейчас.
      this.DS.values = [...res];
      this.DS = {...this.DS};
    })
  }
}
