export interface Sort {
  field: string;
  direction: number; // 0 - вообще нет, -1 возр, 1 убыв.
}

type FilterCb = (el: any, val: any) => boolean; // custom filter

export interface Header {
  name: string;
  field: string;
  filter? : (boolean | FilterCb)
  sortable? : boolean
}

export interface Filter {
  field: string;
  value: any;
  filter? : (boolean | FilterCb)
}

export interface DataProvider {
  header: Header[];
  values: any[];
  pageSize: number;
}
