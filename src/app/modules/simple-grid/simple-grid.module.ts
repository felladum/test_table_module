import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './components/table/table.component';
import { GridHeaderComponent } from './components/table/header/grid-header/grid-header.component';
import {ReactiveFormsModule} from "@angular/forms";
import { PaginationComponent } from './components/table/pagination/pagination.component';



@NgModule({
  declarations: [
    TableComponent,
    GridHeaderComponent,
    PaginationComponent
  ],
  exports: [
    TableComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class SimpleGridModule { }
