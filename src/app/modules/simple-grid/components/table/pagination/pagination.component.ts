import { Component, OnInit } from '@angular/core';
import {DataSourceService} from "../services/data-source.service";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  public pages : number[] = [];
  public currentPage = 0;
  constructor(private dataService : DataSourceService) {

  }

  ngOnInit(): void {

    this.dataService.pages.subscribe((res: number)=>{
      this.pages = [];
      for (let i=0; i<res; i++)
        this.pages.push(i);

    })
    this.dataService.currentPage.subscribe((res: number)=>{
      this.currentPage = res;
    })
  }

  setPage(index: number){
    this.dataService.setPage(index);
  }

}
