import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationComponent } from './pagination.component';
import {DataSourceService} from "../services/data-source.service";
import {BehaviorSubject} from "rxjs";
import {AppComponent} from "../../../../../app.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;
  let DSServiceStub = {
    pages: new BehaviorSubject(2),
    currentPage: new BehaviorSubject(0),
    setPage: function (page: number) {
      return false;
    }
  }
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginationComponent ],
      providers: [{ provide: DataSourceService, useValue:  DSServiceStub}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be two pages', () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelectorAll('li')?.length).toBe(2);
  });

  it('page one should be selected', () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    let isColored = compiled.querySelector('li')?.innerHTML.indexOf('#ccc');
    expect(isColored).toBeGreaterThan(-1);
  });
});
