import { TestBed } from '@angular/core/testing';

import { DataSourceService } from './data-source.service';
import {Filter} from "../../../data-types";

describe('DataSourceService', () => {
  let service: DataSourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataSourceService]
    });
    service = TestBed.inject(DataSourceService);
    // default data
    service.setData({
      header:[
        {
          name: 'Id',
          field: 'id',
          filter: true,
        },
        {
          name: 'wbRating',
          field: 'id',
          filter: true,
        },
        {
          name: 'Отзывов (ф: не больше чем)',
          field: 'reviewsCount',
          filter: function (el, val) {
            return parseInt(el) < parseInt(val) // проверим кастомный поиск
          },
        },
        {
          name: 'sku',
          field: 'sku',
          filter: false
        },
        {
          name: 'Наименование',
          field: 'name',
          filter: true
        },
      ],
      values: [
        {
          "id": "6b6a59e9-de5e-40f5-8b9c-37d980490e1c",
          "wbRating": 1,
          "reviewsCount": 20,
          "nomenclature": 1225024,
          "sku": "SRUK146",
          "name": "Marquez",
          "brandName": "Cathleen",
          "brandId": "c534abf1-d08c-4ba1-b5d6-b1daf93b28f6",
          "image": "http://placehold.it/100x100",
          "preview": "http://placehold.it/300x300",
          "ordered": 85752,
          "soldQuantity": 253,
          "soldAmount": 275922,
          "orderedAmount": 57,
          "availability": 84
        },
        {
          "id": "4d6611f3-7ec7-4d92-a3b8-a063544e3444",
          "wbRating": 0,
          "reviewsCount": 200,
          "nomenclature": 3500182,
          "sku": "SRUK227",
          "name": "Baker",
          "brandName": "Carla",
          "brandId": "e3c9aab4-cda5-49b0-abb8-c96dba27ba9c",
          "image": "http://placehold.it/100x100",
          "preview": "http://placehold.it/300x300",
          "ordered": 239356,
          "soldQuantity": 150,
          "soldAmount": 427567,
          "orderedAmount": 270,
          "availability": 273
        },
        {
          "id": "ec6422a4-2bb5-4579-89d8-91928f2049f2",
          "wbRating": 2,
          "reviewsCount": 227,
          "nomenclature": 2715463,
          "sku": "SRUK369",
          "name": "Charlotte",
          "brandName": "Kelsey",
          "brandId": "b70c3664-3d7b-4945-84a3-1d3409b1481c",
          "image": "http://placehold.it/100x100",
          "preview": "http://placehold.it/300x300",
          "ordered": 414677,
          "soldQuantity": 63,
          "soldAmount": 237105,
          "orderedAmount": 133,
          "availability": 88
        },
      ],
      pageSize: 2
    })
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it( 'pages should be 2', ()=>{
    service.pages.subscribe((r)=> expect(r).toBe(2))
  });

  it( 'filtered pages should be 0', ()=>{
    const filter: Filter = {field: 'name', value: 'xxx'};
    service.setFilter( [filter]) ;
    service.pages.subscribe((r)=> expect(r).toBe(0))
  });

  it( 'multifiltered pages should be 0', ()=>{
    const filter1: Filter = {field: 'name', value: 'a'};
    const filter2: Filter = {field: 'name', value: 'm'};
    service.setFilter( [filter1, filter2]) ;
    service.pages.subscribe((r)=> expect(r).toBe(1))
  });

  it( 'set page should change elems on page to 1', ()=>{
    service.setPage(1);
    service.currentPageData.subscribe((r)=> expect(r.length).toBe(1))
  });
});
