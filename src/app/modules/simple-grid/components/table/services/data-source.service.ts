import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {DataProvider, Filter, Sort} from "../../../data-types";

@Injectable()
// отображение текущей страницы, навешивание фильтров/сортировок
export class DataSourceService {
  private dataSource: DataProvider = {header:[], values:[], pageSize:5};  //new BehaviorSubject<IDataProvider>({header:[], values:[], pageSize:5});
  private currentFilters: Filter[] = [];
  private currentFilteredData: any[] = [];
  public currentSort = new BehaviorSubject<Sort>({field:'', direction:0}) ;
  public currentPageData = new BehaviorSubject<any[]>([]);
  public currentPage = new BehaviorSubject<number>(0);
  public pages = new BehaviorSubject<number>(0);


  constructor() { }

  setData(payload: DataProvider){
      this.dataSource = payload;
      this.applayFilter();
      this.setPage(0);
      this.applayPageData();
  }

  setFilter(newVal: Filter[]){
    this.currentFilters = newVal;
    this.applayFilter();
    this.applayPageData();
  }

  setSort(newSortFieldName?: string){
    const sort = this.currentSort.value;

    if (newSortFieldName){
      if (sort.field === newSortFieldName){
        switch (sort.direction){
          case 0: sort.direction = 1; break;
          case 1: sort.direction = -1; break;
          case -1: sort.direction = 0; break;
        }
      } else {
        sort.field = newSortFieldName;
        sort.direction = 1;
      }
    }

    if (sort.field>'' && sort.direction!=0){
      this.currentFilteredData = this.currentFilteredData.sort((a, b)=>{
        const x = a[sort.field];
        const y = b[sort.field];
        let res = 0;
        if (x < y) {
          res =  -1;
        }
        if (x > y) {
          res = 1;
        }
        return res*sort.direction;
      })
    } else { // исходная сортировка
      this.applayFilter();
    }
    this.currentSort.next(sort);
    this.applayPageData();
  }

  setPage(pageIndex: number){
    this.currentPage.next(pageIndex);
    this.applayPageData();
  }

  private contains(el: string, val: string){
    const reg = new RegExp(val,'i');
    return reg.test(el);
  }

  private applayFilter(){
    const oldPages = this.pages.value;
    this.currentFilters.forEach((f)=>{
        let found = false;
        this.dataSource.header.forEach((h)=>{
          if (h.field == f.field) {
            if (h.filter) {
              if (typeof h.filter == "boolean"){
                f.filter = this.contains; // фильтр по умолчанию - на совпадение подстроки
              } else {
                f.filter = h.filter;
              }
            }
          }
        })

    })

    this.currentFilteredData = this.dataSource.values.filter((el)=>{
      let filtered = true;

      this.currentFilters.forEach((f)=>{
        // @ts-ignore
        filtered = filtered && f.filter(el[f.field], f.value); // здесь по идее уже ТОЛЬКО функция может быть
      });
      return filtered;
    })
    const pages =  Math.ceil(this.currentFilteredData.length / this.dataSource.pageSize);
    let page =  this.currentPage.value;
    if (pages) {
      if (oldPages != pages) {
        if (this.currentPage.value >= pages) {
          page = pages-1;
        }
      }
      this.pages.next(pages);
      this.setPage(page);
    } else{
      this.pages.next(0);
      this.setPage(0);
    }
  }

  private applayPageData(){
    const pageIndex = this.currentPage.value;
    this.currentPageData.next(this.currentFilteredData.slice(this.dataSource.pageSize*pageIndex, this.dataSource.pageSize*(pageIndex+1)))
  }

}
