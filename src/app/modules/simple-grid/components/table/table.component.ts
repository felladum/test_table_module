import {ChangeDetectionStrategy, Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {DataSourceService} from "./services/data-source.service";
import {DataProvider} from "../../data-types";
import {distinctUntilChanged} from "rxjs";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [DataSourceService]
})
export class TableComponent implements OnInit {
  @Input()
  dataProvider: DataProvider = {header:[], values:[], pageSize: 5}
  public currentPage: any[] = [];
  constructor(private dataService : DataSourceService) { }

  ngOnInit(): void {
    this.dataService.currentPageData.pipe(distinctUntilChanged()).subscribe((data: any[]) => {
        this.currentPage = data;
    });
  }

  ngOnChanges(changes: SimpleChanges){
    this.dataService.setData(changes['dataProvider'].currentValue);
  }
}
