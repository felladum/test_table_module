import {Component, Input, OnInit, OnChanges, SkipSelf} from '@angular/core';
import {Header, Sort} from "../../../../data-types";
import {DataSourceService} from "../../services/data-source.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Filter} from "../../../../data-types";

// TODO по хорошему нужна валидация данных фильтра, в зависимости от типа колонки / либо отдельно задавать в настройках фильтра

@Component({
  selector: '[app-grid-header]',
  templateUrl: './grid-header.component.html',
  styleUrls: ['./grid-header.component.scss']
})
export class GridHeaderComponent implements OnInit {
  @Input() Headers: Header[] = [];
  public currentSort: Sort = {field:'', direction:0};
  public FG: FormGroup;
  constructor(private dataService : DataSourceService) {
    this.FG = new FormGroup({});
  }

  ngOnInit(): void {
    this.onFilterChanges();
    this.onSortChange();
  }



  ngOnChanges(){
    const group: any = {};
    this.Headers.forEach((el)=>{
        if(el.filter){
          group[el.field] = new FormControl()
        }
    })
    this.FG = new FormGroup(group);
  }

  setSort(fieldName: string){
    this.dataService.setSort(fieldName);
  }

  onSortChange(){
    this.dataService.currentSort.subscribe((res: Sort)=>{
        this.currentSort = res;
      }
    );
  }

  onFilterChanges(){
    this.FG.valueChanges.subscribe(res=>{
      const filters : Filter[] = [];
      for(var key in res){
        if (res[key]){
          filters.push({
            field: key,
            value: res[key]
          })
        }
      }
      this.dataService.setFilter(filters);
    });
  }

}
